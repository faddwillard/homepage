using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParkingLibrary;
using System.Threading;

namespace ParkingApp
{
    class Program
    {
        static void Print(string message, int delay)
        {
            Thread.Sleep(delay);
            Console.WriteLine(message);
        }
        static void Main(string[] args)
        {
            Console.CursorVisible = false;

            Console.WriteLine("Loading... ");
            Print("Loading system...", 1000);
            Print("Трошки почекаєм,окей?", 500);
            Print("И т.д.", 2000);

            int pos = Console.CursorTop;
            Console.SetCursorPosition(11, 0);
            Console.Write("OK");
            Console.SetCursorPosition(0, pos);

            var rand = new Random();

            for (int i = 0; i <= 100; i++)
            {
                if (i < 25)
                    Console.ForegroundColor = ConsoleColor.Red;
                else if (i < 50)
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                else if (i < 75)
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                else if (i < 100)
                    Console.ForegroundColor = ConsoleColor.Yellow;
                else
                    Console.ForegroundColor = ConsoleColor.Green;

                string pct = string.Format("{0,-30} {1,3}%", new string((char)0x2592, i * 30 / 100), i);
                Console.CursorLeft = 0;
                Console.Write(pct);
                Thread.Sleep(rand.Next(0, 200));
            }
            Console.WriteLine();
            Console.ResetColor();
            Console.WriteLine();

            string text = "PARKING EMULATOR";
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            var width = Console.WindowWidth;
            var padding = width / 2 + text.Length / 2;
            Console.WriteLine("{0," + padding + "}", text);
            Console.ResetColor();
            Console.WriteLine();
            bool alive = true;
            
                while (alive)
                {
                try
                {
                    ConsoleColor color = Console.ForegroundColor;
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    Console.WriteLine("1. Add new car \t 2. Show parking's balance \t 3. Remove the car");

                    Console.WriteLine("4. Show all cars \t 5. Exit \t 6. Add money to car ");

                    Console.WriteLine("7. Last minute of TRANSACTIONS \t");
                    Console.WriteLine("Input your choice:");
                    Console.ForegroundColor = color;

                    int choise = Convert.ToInt32(Console.ReadLine());

                    switch (choise)
                    {
                        case 1:
                            Console.WriteLine();
                            Console.Write("Enter balance: ");
                            double balance = Convert.ToDouble(Console.ReadLine());
                            Console.WriteLine("Car MENU: \n -Passenger \n -Truck \n -Bus \n -Motorcycle");
                            Console.Write("Enter CarType: ");
                            string carType = Console.ReadLine();
                            Car newCar = new Car(balance, (CarType)Enum.Parse(typeof(CarType), carType));
                            Parking.Instance.AddCar(newCar);
                            break;
                        case 2:
                            Console.Write("Parking BALANCE: ");
                            Console.WriteLine(Parking.Instance.GetTotalRevenue());
                            break;
                        case 3:
                            Console.Write("Enter id for remove: ");
                            var carId = Guid.Parse(Console.ReadLine());
                            if (Parking.Instance.RemoveCar(carId))
                            {
                                Console.WriteLine("Car removed successfully");
                            }
                            else
                            {
                                Console.WriteLine("Car not removed");
                            }
                            break;
                        case 4:
                            Console.WriteLine("Car list: ");
                            var cars = Parking.Instance.CarList;
                            foreach (var car in cars)
                            {
                                Console.WriteLine(car);
                            }
                            break;
                        case 5:
                            alive = false;
                            continue;

                        case 6:
                            Console.WriteLine();
                            Console.Write("Enter the amount of money: ");
                            double amount = Convert.ToDouble(Console.ReadLine());
                            Console.Write("Please enter Car id: ");
                            var carToAddMoneyId = Guid.Parse(Console.ReadLine());
                            Parking.Instance.AddMoneyToCar(carToAddMoneyId, amount);
                            break;
                        case 7:
                            Console.WriteLine("List of last minute TRANSACTIONS: ");
                            foreach (var transaction in Parking.Instance.LastMinuteTransactions)
                            {
                                Console.WriteLine(transaction);
                            }
                            break;


                        default:
                            Console.WriteLine("Please enter the number");
                            break;
                    }
                }
                catch (Exception ex)
                {
                    ConsoleColor color = Console.ForegroundColor;
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(ex.Message);
                    Console.ForegroundColor = color;
                }
            }
        }
    }
}