﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingLibrary
{
    public class Car
    {
        private readonly double Fine;

        //First hour balance is free
        
        public double Balance { get; set; } = 1200;

        public Car(double balance, CarType type)
        {
            this.Id = Guid.NewGuid();
            this.Type = type;
            this.Balance = balance;
            this.Fine = Settings.Fine * Settings.Prices[type];
        }

        public CarType Type { get; }

        public Guid Id { get; }

        public bool BalanceIsPositive
        {
            get
            {
                return this.Balance >= 0;
            }
        }

        public void AddMoneyToBalance(double amountToAdd)
        {
            if (Balance < 0)
            {
                var negativePart = -Balance;

                amountToAdd -= negativePart * Fine;

                if (amountToAdd >= 0)
                    Balance += amountToAdd;
                else
                    Balance += amountToAdd / Fine;
            }

            Balance += amountToAdd;
        }

        public void ChargeMoneyForParking(double chargingAmount)
        {
            Balance -= chargingAmount;
        }

        public override string ToString()
        {
            return $"{Id} - {Balance} - {Type}";
        }
    }
}
