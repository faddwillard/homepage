﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Reactive.Linq;

namespace ParkingLibrary
{
    public sealed class Parking
    {
        private static readonly Lazy<Parking> Lazy = new Lazy<Parking>(() => new Parking());

        private readonly IObservable<long> _carBalanceChargingObservable = Observable.Interval(TimeSpan.FromMilliseconds(Settings.Period));
        private readonly IObservable<long> _loggingTransactionObservable = Observable.Interval(TimeSpan.FromMinutes(1));

        private List<Car> vehicles = new List<Car>();
        private List<Transaction> transactions = new List<Transaction>();


        public Parking()
        {
            _carBalanceChargingObservable.Subscribe(t => ChargeMoneyFromCars());
            _loggingTransactionObservable.Subscribe(t => LogLastMinutesTransactions());
        }

        public static Parking Instance => Lazy.Value;

        public double GetTotalRevenue() => transactions.Sum(sum => sum.Amount);

        public IReadOnlyCollection<Car> CarList => vehicles.AsReadOnly();

        private void ChargeMoneyFromCars()
        {
            foreach (var car in CarList.ToList())
            {
                var amountToChange = Settings.Prices[car.Type];
                car.ChargeMoneyForParking(amountToChange);
                transactions.Add(new Transaction(DateTime.UtcNow, car.Id, amountToChange));
            }
        }

        private void LogLastMinutesTransactions()
        {
            using (var fileStream = File.AppendText("Transactions.log"))
            {
                var lastMinuteTransactions = LastMinuteTransactions;

                var lastMinuteSum = lastMinuteTransactions.Sum(x => x.Amount);
                var lastMinuteStartTime = DateTime.Now.Subtract(TimeSpan.FromMinutes(1));
                fileStream.WriteLine($"{lastMinuteStartTime} : {lastMinuteSum}");
            }
        }

        public bool AddCar(Car car)
        {
            if (vehicles.Count < Settings.ParkingSpace)
            {
                vehicles.Add(car);
                return true;
            }
            return false;
        }

        public bool RemoveCar(Guid carId)
        {
            var car = vehicles.FirstOrDefault(c => c.Id == carId);

            return RemoveCar(car);
        }

        public bool RemoveCar(Car car)
        {
            if (car == null || !vehicles.Contains(car))
                return false;

            if (!car.BalanceIsPositive)
                return false;

            vehicles.Remove(car);
            return true;
        }

        public Car GetCarById(Guid id)
        {
            return vehicles.FirstOrDefault(c => c.Id == id);
        }

        public void AddMoneyToCar(Guid carId, double balance)
        {
            var car = vehicles.FirstOrDefault(c => c.Id == carId);

            if (car != null)
            {
                car.AddMoneyToBalance(balance);
            }
        }

        public List<Transaction> LastMinuteTransactions => transactions.Where(tr => tr.DateOfCreation >= DateTime.UtcNow.AddMinutes(-1)).ToList();

        public int FreeSpaces => Settings.ParkingSpace - vehicles.Count;
    } 

    
}