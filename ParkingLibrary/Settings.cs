﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingLibrary
{
    public static class Settings
    {
        public static int Period = 5000;
        public static int ParkingSpace = 10;
        public static double Fine = 2.5;
        public static Dictionary<CarType, double> Prices = new Dictionary<CarType, double>
        {
            [CarType.Passenger] = 2,
            [CarType.Truck] = 5,
            [CarType.Bus] = 3.5,
            [CarType.Motorcycle] = 1
        };
    }
}
