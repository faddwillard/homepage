﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingLibrary
{
    public class Transaction
    {
        public DateTime DateOfCreation { get; }
        public Guid CarId { get; }
        public double Amount { get; }

        public Transaction(DateTime time, Guid carId, double amount)
        {
            this.DateOfCreation = time;
            this.CarId = carId;
            this.Amount = amount;
        }

        public override string ToString()
        {
            return $"{DateOfCreation} - {CarId} - {Amount}";
        }
    }
}
